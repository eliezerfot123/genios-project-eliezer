import pandas as pd
import openai
import asyncio

# Import the data
df = pd.read_csv('DataRedesSociales.csv')

# Set the API key
API_KEY = "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"
openai.api_key = API_KEY

async def async_question_davinci_model(text: str):
    """
    Asynchronous function to create a question from a text prompt.

    Args:
        text: The text prompt.

    Returns:
        The question created from the text prompt.
    """

    temas = {"gobierno", "oposición", "economia", "salud"}

    response = openai.Completion.create(
        prompt=f"¿Qué sentimiento tiene el texto siguiente? {text} y categoriza los por siguientes temas: {temas}",
        temperature=0.7,
        max_tokens=50,
        engine="text-davinci-003",
    )

    return response["choices"][0]["text"]


async def main():
    # Iterate over the DataFrame
    for _, row in df.iterrows():
        # Get the text
        text = row["text"]

        # Get the feeling
        feeling = await async_question_davinci_model(text)

        # Update the DataFrame
        df.loc[_, "feeling"] = feeling

    # Sort the DataFrame by feeling
    df_sorted = df.sort_values(by="feeling")

    # Save the DataFrame
    df_sorted.to_csv("data_sorted_by_feeling_v5.csv")

asyncio.run(main())
