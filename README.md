# genios_projects



## Getting started

Folder for backend apps/

Folder for frontend front/


execute test script  you can install PIPENV

> pipenv install -r requirements/base.txt
> pipenv install -r requirements/local.txt

inside your environment

> pipenv shell

ready to start test script

### Run backend first

If you want build a docker container for local development:

> docker compose -f local.yml build 

For up:

> docker compose -f local.yml up

For see proccess

> docker compose -f local.yml ps 

For not repeat -f local.yml

> export COMPOSE_FILE=local.yml
> docker compose build
> docker compose ps 
> docker compose up 
> docker compose down

Administrative command to interact Note: The --rm is used to kill the container, otherwise the container is left running.

> docker compose run --rm django COMMANDO
> docker compose run --rm django python manage.py createsuperuser

to run django debugin normally enable debugger:

> docker-compose up # we run 
> docker compose ps # we see id 
> docker rm -f # kill the django service
> docker compose run --rm --service-ports django #run django 

separately for our debuggins 
Remember default docker commands

> docker container
> docker images
> docker volume
> docker network
> docker rm
> docker -q 

# run frontend

First option

> npm run dev

Second option

> docker compose up

## Authors and acknowledgment
- Eliezer Romero
## License
For open source projects, say how it is licensed.

## Project status
80% Complete
