import json

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated


class ShowData(APIView, ):
    permission_classes = (IsAuthenticated,)
    def get(self, request, format=None):
        #for this api read json and send it
        with open("output.json", "r") as json_file:
            data = json.load(json_file)

        return Response(data)
