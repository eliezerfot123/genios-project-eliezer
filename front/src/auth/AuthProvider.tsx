import { useContext, createContext, useState, useEffect } from "react";
import type { AuthResponse, AccessTokenResponse, User } from "../types/types";
import { API_URL } from "../auth/constants";

interface AuthProviderProps {
    children: React.ReactNode;
}

const AuthContext = createContext({
    isAuthenticate: false,
    getAccessToken: () => {},
    saveUser: (userData: AuthResponse) => {},
    getRefreshToken: () => {},
    getUser: () => ({} as User | undefined),
    signOut: () => {},

});

export function AuthProvider({ children }: AuthProviderProps) {
    const [isAuthenticate, setIsAuthenticate] = useState(false);
    const [accessToken, setAccessToken] = useState<string>("");
    const [user, setUser] = useState<User>();
    const [isLoading, setIsLoading] = useState(true);
    //const [refreshToken, setRefreshToken] = useState<string>("");



    async function requestNewAccessToken(refreshToken:string){
        try {
            const response = await fetch(`${API_URL}/token/refresh/`, {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": `Bearer ${refreshToken}`,
                },
            });
            if(response.ok){
                const json = await response.json() as AccessTokenResponse;
                if(json.error){
                    throw new Error(json.error);
                }
                return json;
            }else{
                throw new Error(response.statusText);
            }

        } catch (error){
            console.log(error);
            return null;
        }

    }

    async function getUserInfo(accessToken:string){
        try {
            const response = await fetch(`${API_URL}/users/`, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${accessToken}`,
                },
            });
            if(response.ok){
                
                const json = await response.json();
                if(json.error){
                    throw new Error(json.error);
                }
                return json.name;
            }else{
                throw new Error(response.statusText);
            }

        } catch (error){
            console.log(error);
            return null;
        }        
    }

    // create function for check auth for user
    async function checkAuth(){
        if(accessToken){
            // the user is authenticated
            const userInfo = await getUserInfo(accessToken);
            console.log("hola")
            setUser(userInfo);
            setAccessToken(accessToken);
            setIsAuthenticate(true);
        }else {
            console.log("hola2")

            // this user is not authenticated
            const token = getRefreshToken();
            if(token){
                // the user has a token refresh
                const newAccessToken = await requestNewAccessToken(token);
                if(newAccessToken){
                    const userInfo = await getUserInfo(newAccessToken);
                    if(userInfo){
                        saveSessionInfo(userInfo, newAccessToken, token);
                        setIsLoading(false);
                        return;
                    }
                }
            }
        }
        setIsLoading(false);
    }

    function signOut(){
        setIsAuthenticate(false);
        setAccessToken("");
        setUser(undefined);
        localStorage.removeItem("token");
    }

    // create function for save session info
    function saveSessionInfo(
        userInfo:User, 
        accessToken:string, 
        refreshToken:string
    ){
        setAccessToken(accessToken);
        setUser(userInfo);
        localStorage.setItem("token", JSON.stringify(refreshToken));
        setIsAuthenticate(true);
    }

    function getAccessToken() {
        return accessToken;
    }

    function getRefreshToken():string|null {
        const tokenData = localStorage.getItem("token");
        if(tokenData){
            const { token } = JSON.parse(tokenData);
            return token;
        }
        return null;
    }
    
    function saveUser(userData: AuthResponse){
        saveSessionInfo(userData.user, userData.access, userData.refresh);

    }

    function getUser(): User | undefined {
        return user;
    }

    useEffect(() => {
        checkAuth();
      }, []);

    return (
        <AuthContext.Provider 
            value={{
                isAuthenticate, 
                getAccessToken, 
                saveUser,
                getUserInfo,
                getRefreshToken, 
                getUser,
                signOut
            }}
        >
            {isLoading? <div>Cargando...</div> : children}
        </AuthContext.Provider>
    );
}

export const useAuth = () => useContext(AuthContext);