import { useState } from "react"
import DefaultLayout from "../layout/DefaultLayout"
import { useAuth } from "../auth/AuthProvider"
import { Navigate, useNavigate } from "react-router-dom"
import { API_URL } from "../auth/constants";
import { AuthResponserError,  } from "../types/types";

export default function Signup() {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [errorResponse, setErrorResponse] = useState('');

    /* Valid is authendicated */
    const auth = useAuth();
    const goTo = useNavigate();

    if(auth.isAuthenticate){
        return <Navigate to="/dashboard" />
    }

    async function handleSubmit(e: React.FormEvent<HTMLFormElement>){
        e.preventDefault();

        try {
            const response = await fetch(`${API_URL}/register/`, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    name,
                    email, 
                    password,
                    password2
                }),
                
            });
            if(response.ok) {
                console.log("Se Creo el usuario");
                setErrorResponse("");
                goTo("/");
            }
            else{
                console.log("No se creo el usuario")
                const json = await response.json() as AuthResponserError;
                setErrorResponse(json.body.error);
                return;
            }
        } catch (e) {
            console.log(e)
        }

    }

    return (
        <DefaultLayout>
            <form className="form" onSubmit={handleSubmit}>
                <h1>Signup</h1>
                { !! errorResponse && <div className="errorMessage">{ errorResponse }</div>}

                <label>Nombre y apellido</label>
                <input type="text"
                value={name}
                onChange={e => setName(e.target.value)}
                />

                <label>Email</label>
                <input type="text"
                value={email}
                onChange={e => setEmail(e.target.value)}
                />

                <label>Password</label>
                <input type="password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                />

                <label>Repita password</label>
                <input type="password"
                value={password2}
                onChange={e => setPassword2(e.target.value)}
                />

         

                <button>Registrarse</button>
            </form>
        </DefaultLayout>
    )
}