import { useAuth } from "../auth/AuthProvider";
import { useEffect, useState } from "react";
import { API_URL } from "../auth/constants";
import ProtLayout from "../layout/ProtLayout"
import { flexRender, getCoreRowModel, useReactTable, getPaginationRowModel } from "@tanstack/react-table";


interface User {
    id: string;
    name: string;
    email: string;
    is_staff: boolean;
    is_active: boolean;
  }

interface Data{
    id: string;
    text: string;
    sentimientos: string;
    emociones: string;
    temas: string;
}


  
export default function Dashboard() {
    const auth = useAuth();
    const [user, setUser] = useState<User[]>([]);
    const [data, setData] = useState<Data[]>([]);

    const columns = [
      {
        accessorKey: 'Texto',
      },
      {
        accessorKey: 'Sentimientos',

      },
      {
        accessorKey: 'Emociones',
      },
      {
        accessorKey: 'Temas',
      },
    
    ];



    if(auth.isAuthenticate){
        console.log("estoy autenticado")
    }


    async function getUser() {
        const accessToken = auth.getAccessToken();
        try {
          const response = await fetch(`${API_URL}/users/`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${accessToken}`,
            },
          });
    
          if (response.ok) {
            const json = await response.json();
            setUser(json);
          }
        } catch (error) {
          console.log(error);
        }
      }

      async function getData() {
        const accessToken = auth.getAccessToken();
        try {
          const response = await fetch(`${API_URL}/core/data/`, {
            method: "GET",
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${accessToken}`,
            },
          });
    
          if (response.ok) {
            const json = await response.json();
            setData(json);
          }
        } catch (error) {
          console.log(error);
        }
      }

      const table = useReactTable({
        data,
        columns,
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
      })

    useEffect(() => {
        getUser();
        getData();
    }, []);


    return (
        <ProtLayout>
            <h1>
                 
                {user.map((user) => (
                    <div key={user}>Bienvenido {user.name}</div>
                ))}
            </h1>
            <hr/>

            <div className="px-6 py-4">
              <table className="table-auto w-full">
                  <thead>
                    {table.getHeaderGroups().map((headerGroup) => (
                      <tr key={headerGroup.id} className="border-b border-gray-300">
                        {headerGroup.headers.map(header => (
                          <th key={header.id}>
                              {header.isPlaceholder 
                                ? null 
                                :flexRender(
                                  header.column.columnDef.header,
                                  header.getContext()
                                )
                              }

                          </th>
                        ))}
                      </tr>
                    ))}
                  </thead>
                  <tbody>
                  {data.map((data) => (
                    <tr key={data.id}>
                      <td>{data.text}</td>
                      <td>{data.sentimientos}</td>
                      <td>{data.emociones}</td>
                      <td>{data.temas}</td>
                    </tr>
                  ))}
                  </tbody>
              </table>
            </div>

          

            
            
        </ProtLayout>
    )
}