import { Link } from "react-router-dom";
import React, { MouseEvent } from "react";
import { useAuth } from "../auth/AuthProvider";
import { useNavigate } from "react-router-dom";

interface ProtLayoutProps {
    children?: React.ReactNode;
  }

export default function ProtLayout({ children }: ProtLayoutProps){
    const auth = useAuth();
    const goTo = useNavigate();

    async function handleSignOut(e: MouseEvent) {
        e.preventDefault();
        auth.signOut();
        goTo("/");
      }

    return (
        <>
          <header>
            <nav>
              <ul>
                <li>
                  <Link to="/dashboard">Dashboard</Link>
                </li>

                <li>
                  <Link to="/me">{auth.getUser()?.email ?? ""}</Link>
                </li>
                <li>
                  <a href="#" onClick={handleSignOut}>
                    Sign out
                  </a>
                </li>
              </ul>
            </nav>
          </header>
    
          <main>{children}</main>
        </>
      );
    }
