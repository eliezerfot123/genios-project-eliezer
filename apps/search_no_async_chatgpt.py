import pandas as pd
import openai
import asyncio

# Import the data
df = pd.read_csv('DataRedesSociales.csv')

# Set the API key
API_KEY = "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"
openai.api_key = API_KEY

def question_davinci_model(text: str):
    temas = {"gobierno", "oposición", "economia", "salud"}
    response = openai.Completion.create(
        prompt=f"¿Qué sentimiento tiene el texto siguiente? {text} y separa los por siguientes temas: {temas}",
        temperature=0.7,
        max_tokens=200,
        engine="text-davinci-003",
    )
    return response["choices"][0]["text"]


df["feeling"] = df["text"].apply(question_davinci_model)

df_sorted = df.sort_values(by="feeling")

df_sorted.to_csv("data_sorted_by_feeling.csv")
