import React, { useState } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import DefaultLayout from "../layout/DefaultLayout"
import { useAuth } from "../auth/AuthProvider";
import { API_URL } from "../auth/constants";
import { AuthResponserError, AuthResponse } from "../types/types";


export default function Login() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [errorResponse, setErrorResponse] = useState('');
    
    /* Valid is authendicated */
    const auth = useAuth()
    const goTo = useNavigate();

    if(auth.isAuthenticate){
        return <Navigate to="/dashboard" />
    }

    async function handleSubmit(e: React.FormEvent<HTMLFormElement>){
        e.preventDefault();

        try {
            const response = await fetch(`${API_URL}/token/`, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    email, 
                    password,
                }),
            });

            if(response.ok) {
                console.log("Login Successful");
                setErrorResponse("");
                
                const json = (await response.json()) as AuthResponse;
                if(json.access && json.refresh){
                    auth.saveUser(json);
                    goTo("/dashboard");
                }
                //auth.login(json.token);
                
            }
            else{
                console.log("Ah Ocurrido un error!")
                const json = await response.json() as AuthResponserError;
                setErrorResponse(json.body.error);
                return;
            }
        } catch (e) {
            console.log(e)
        }

    }

    
    return (
        <DefaultLayout>
            <form className="form" onSubmit={handleSubmit}>
                <h1>Login</h1>
                { !! errorResponse && <div className="errorMessage">{ errorResponse }</div>}

                <label>Email</label>
                <input type="text"
                value={email}
                onChange={e => setEmail(e.target.value)}
                />

                <label>Password</label>
                <input type="password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                />
                <button>Iniciar Sesión</button>
            </form>
        </DefaultLayout>
    )
}