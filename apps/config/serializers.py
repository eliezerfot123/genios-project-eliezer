import logging

from django.apps import apps
from rest_framework import status
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.exceptions import AuthenticationFailed
from apps.users.api.serializers import UserDetailSerializer


logger = logging.Logger(__name__)


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    """
    Custom serializer for get token JWT, pass user_id and business_id
    """

    def validate(self, attrs):
        data = super().validate(attrs)
        logger.debug(f"TokenObtain: {data} user: {self.user}")
        data["user_id"] = self.user.id
        data["email"] = self.user.email
        return data
