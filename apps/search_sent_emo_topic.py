import openai
import pandas as pd
import asyncio
import ipdb; ipdb.set_trace()

# Importar los datos
df = pd.read_csv('DataRedesSociales.csv')

# Establecer la clave de API
API_KEY = "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"

# Crear una función de clasificación


def sentimientos(text):
    openai.api_key = API_KEY

    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"¿Qué sentimiento tiene el texto siguiente?? {text} ",
        temperature=0.5,
        max_tokens=100,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return response["choices"][0]["text"]

def emociones(text):
    openai.api_key = API_KEY

    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"¿Qué emociones tiene el texto siguiente?? {text} ",
        temperature=0.5,
        max_tokens=100,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return response["choices"][0]["text"]

def temas(text):
    openai.api_key = API_KEY
    temas = {"gobierno", "oposición", "economia", "salud"}
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=f"Clasifica los siguientes textos {text} por los temas {temas} ",
        temperature=0.5,
        max_tokens=100,
        top_p=1,
        frequency_penalty=0,
        presence_penalty=0
    )
    return response["choices"][0]["text"]

df["sentimientos"] = df["text"].apply(sentimientos)
df["emociones"] = df["text"].apply(emociones)
df["temas"] = df["text"].apply(temas)
#df.to_csv("data_with_topics.csv")

json_data = df.to_json(orient="records")

with open("output.json", "w") as outfile:
    outfile.write(json_data)
